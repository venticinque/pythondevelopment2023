import sys
import cowsay
from argparse import ArgumentParser


if len(sys.argv) == 1:
    message = input()
    print(cowsay.cowsay(message))
else:
    parser = ArgumentParser()
    parser.add_argument("message", type=str, nargs="?")
    parser.add_argument("-e", "--eye_string", default=cowsay.Option.eyes)
    parser.add_argument("-f", "--cowfile")
    parser.add_argument("-l", action="store_true")
    parser.add_argument("-n", action="store_false")
    parser.add_argument("-T", "--tongue_string", default=cowsay.Option.tongue)
    parser.add_argument("-W", "--column", default=40, type=int)
    parser.add_argument("-bdgpstwy", dest="preset")
    args = parser.parse_args()
    
    if args.l:
        print(cowsay.list_cows())
    else:
    	cows = cowsay.cowsay(args.message, preset=args.preset, eyes=args.eye_string, 
tongue=args.tongue_string,width=args.column, wrap_text=args.n, cowfile=args.cowfile)
    	print(cows)
