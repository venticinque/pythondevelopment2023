import random
import sys
import urllib.request

def call_count(func):
    def helper(*args):
        helper.calls += 1
        return func(*args)
    helper.calls = 0
    return helper

def bullcows(guess: str, secret: str) -> (int, int):
	cow = len([x for x in set(guess) if x in set(secret)])
	bull = len([x for y, x in enumerate(secret) if y < len(guess) and secret[y] == guess[y]])
	return (bull, cow)

@call_count
def ask(promt: str, valid: list[str] = None) -> str:
	s = input(promt)
	if valid != None:
		while s not in valid:
			s = input(promt)
	return s

def inform(format_string: str, bulls: int, cows: int) -> None:
	print(format_string.format(bulls, cows))

def gameplay(ask: callable, inform: callable, words: list[str]) -> int:
	secret = random.choice(words)
	guess = ""
	while guess != secret:
		guess = ask("Введите слово: ", words)
		b, c = bullcows(guess, secret)
		inform("Быки: {}, Коровы: {}", b, c)
	return ask.calls

if __name__ == "__main__":
	word_len = 5
	if len(sys.argv) != 2 and len(sys.argv) != 3:
		raise ValueError("python3 -m bullcows word_dict word_len = 5")
	if len(sys.argv) == 3:
		word_len = sys.argv[2]
	words_path = sys.argv[1]
	try:
		with urllib.request.urlopen(words_path) as words:
			words = words.read().decode().split()
			if word_len:
			    words = [word for word in words if len(word) == int(word_len)]
	except Exception:
		with open(words_path) as words:
			words = words.read().split()
			if word_len:
			    words = [word for word in words if len(word) == int(word_len)]
	print(gameplay(ask, inform, words))